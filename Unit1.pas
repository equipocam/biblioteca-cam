unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, jpeg, Db, DBTables;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Bevel1: TBevel;
    Label1: TLabel;
    c1: TComboBox;
    Label2: TLabel;
    e1: TEdit;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    usuario_con: TTable;
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure c1Change(Sender: TObject);
    procedure e1KeyPress(Sender: TObject; var Key: Char);
    procedure c1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  con:integer=3;
implementation

uses Unit3, Unit2;



{$R *.DFM}

procedure TForm1.SpeedButton3Click(Sender: TObject);
begin
if application.MessageBox('�Desea salir de la aplicacion?','SALIR',mb_yesno+mb_iconquestion)=id_yes then
application.Terminate;
end;

procedure TForm1.SpeedButton2Click(Sender: TObject);
begin
if(e1.Text='')  then
begin
application.MessageBox('CAMPO YA ESTA VACIO','USUARIO CONTRASE�A',mb_ok+mb_iconwarning);
c1.Text:='Elegir usuario';
e1.SetFocus;
end
else
begin
if application.MessageBox('�DESEAS LIMPIAR LOS CAMPOS?','USUARIO CONTRASE�A',mb_yesno+mb_iconquestion)=id_yes then
begin
e1.Clear;
c1.Text:='Elegir usuario';
e1.Visible:=true;
label2.Visible:=true;
end;
end;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
var
a:boolean;
begin
a:=false;
if(c1.Text='Invitado') then
begin
application.MessageBox('BIENVENIDO AL SISTEMA','BIEMVENIDA',MB_OK+MB_ICONINFORMATION);
form3.show;
FORM3.deudores.TabVisible:=false;
form3.profes.TabVisible:=false;
form3.bajas.TabVisible:=false;
form3.cuenta.TabVisible:=false;
form3.alta.TabVisible:=false;
form1.Hide;
end
else
begin
if c1.Text='Elegir usuario' then
begin
application.MessageBox('No se selecciono ningun usuario','MENSAJE',mb_ok+mb_iconerror);
c1.SetFocus;
end
else
begin
if(e1.Text='') then
begin
application.MessageBox('Campo contrase�a no llenado','MENSAJE',mb_ok+mb_iconerror);
e1.SetFocus;
end
else
begin
usuario_con.First;
while not usuario_con.Eof do
begin
if(c1.Text=usuario_con.FieldValues['Usuario']) and (uppercase(e1.Text)=usuario_con.FieldValues['Contrase�a'])then
begin
application.MessageBox('CONTRASE�A CORRECTA','CONTRASE�A',MB_OK+MB_ICONINFORMATION);
a:=true;
form3.show;
form1.Hide;
break;
end
else
usuario_con.Next;
end;
if(a=false) then
begin
application.MessageBox('CONTRASE�A INCORRECTA','CONTRASE�A',MB_OK+mb_ICONERROR);
con:=con-1;
label3.Left:=145;
label3.Caption:='Intentos restantes '+inttostr(con);
e1.Clear;
c1.SetFocus;
end;
if(con=0) then
begin
application.MessageBox('INTENTOS AGOTADOS LA APLICACION SE CERRARA','INGRESO',MB_OK+MB_ICONINFORMATION);
application.Terminate;
end;
end;
end;
end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
usuario_con.databasename:=getcurrentdir+'\';
usuario_con.TableName:='usuario_contrase�a.db';
usuario_con.Active:=true;
end;

procedure TForm1.c1Change(Sender: TObject);
begin
if c1.Text='Invitado' then
begin
label3.Visible:=false;
e1.Visible:=false;
label2.Visible:=false;
end
else
begin
label3.Visible:=true;
e1.Visible:=true;
label2.Visible:=true;
end;
end;

procedure TForm1.e1KeyPress(Sender: TObject; var Key: Char);
begin
if not(key in ['0'..'9','�','�','A'..'Z','a'..'z',''#8, #13]) then
begin
key:=#0;
end;
if key in [#13] then
begin
speedbutton1.Click;
end;
end;

procedure TForm1.c1KeyPress(Sender: TObject; var Key: Char);
begin
if not(key in []) then
key:=#0;
end;

end.
